/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.time.LocalDate;
import ufps.util.colecciones_seed.Pila;

/**
 *
 * @author madar
 */
public class Proveedor {
    
    private int id_proveedor;
    private String nombre;
    private Pila<Vacuna> vacunas=new Pila();

    public Proveedor() {
    }

    public Proveedor(int id_proveedor, String nombre) {
        this.id_proveedor = id_proveedor;
        this.nombre = nombre;
    }

//    public Proveedor(int id_proveedor, String nombre, int canVacunas, String fecha) {
//        this.id_proveedor = id_proveedor;
//        this.nombre = nombre;
//        this.crearVacunas(canVacunas, fecha);
//    }
    
    public Proveedor(int id_proveedor, String nombre, int canVacunas, String fecha, int vacunasResgistradas) {
        this.id_proveedor = id_proveedor;
        this.nombre = nombre;
        this.crearVacunas(canVacunas, fecha,vacunasResgistradas);
    }
    
    
    
    
    private void crearVacunas(int canVacunas, String fecha, int vacunasResgistradas)
    {
        String datos[]=fecha.split("/");
        LocalDate exp=LocalDate.of(Integer.parseInt(datos[2]), Integer.parseInt(datos[1]), Integer.parseInt(datos[0]));
        for(int i=1;i<=canVacunas;i++)
        {
            int codigo=vacunasResgistradas+i; //esto hay que usarlo
            this.vacunas.apilar(new Vacuna(codigo,exp));
        }
    }
    
    
    public int getId_proveedor() {
        return id_proveedor;
    }

    public void setId_proveedor(int id_proveedor) {
        this.id_proveedor = id_proveedor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Pila<Vacuna> getVacunas() {
        return vacunas;
    }

    public void setVacunas(Pila<Vacuna> vacunas) {
        this.vacunas = vacunas;
    }

    @Override
    public String toString() {
        return "\n Proveedor{" + "id_proveedor=" + id_proveedor + ", nombre=" + nombre + ", mis vacunas son:"+this.getListadoVacunas()+" }\n\n";
    }
    
    /**
     *  WARNING!!!!--> PILA SE BORRA
     * @return una cadena con el listado de vacunas
     */
    public String getListadoVacunas() {
        String msg = "";
        while (!this.vacunas.esVacia()) {
            msg += this.vacunas.desapilar().toString() + "\t";
        }
        return msg;
    }
    
    
    
}
